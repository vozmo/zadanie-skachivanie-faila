﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Net;
using System.Windows.Forms;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;

namespace Скачивание_файла
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string url = urlBox.Text;
            //ChoiceFolder:
            var saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FileName = System.IO.Path.GetFileName(url);
            DialogResult result = saveFileDialog1.ShowDialog();
            //DownloadFile:
            WebClient Cl = new WebClient();
            Cl.DownloadProgressChanged += new DownloadProgressChangedEventHandler(Cl_DownloadProgressChanged);
            try
            {
                Cl.DownloadFileAsync(new Uri(url), saveFileDialog1.FileName);
            }
            catch (System.UriFormatException)
            {
                System.Windows.Forms.MessageBox.Show("Не указана ссылка на файл");
            }
        }
        void Cl_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            /* if() System.Windows.Forms.MessageBox.Show("Нет интернет соединения"); */
            try
            {
                Ping p = new Ping();
                String h = "google.com";
                byte[] buffer = new byte[32];
                int t = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = p.Send(h, t, buffer, pingOptions);
                if (reply.Status != IPStatus.Success)
                {
                    System.Windows.Forms.MessageBox.Show("Нет интернет соединения");
                }
            }
            catch (PingException)
            {
                System.Windows.Forms.MessageBox.Show("Нет интернет соединения");
            }
            progressBar1.Maximum = (int)e.TotalBytesToReceive / 1000;
            progressBar1.Value = (int)e.BytesReceived / 1000;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
